let number1 = document.getElementById('txt-number1');
let number2 = document.getElementById('txt-number2');
let myResult = 0;

// Buttons for operations
let btnSum = document.getElementById('btn-sum');
let btnSubstract = document.getElementById('btn-substract');
let btnMultiply = document.getElementById('btn-multiply');
let btnDivide = document.getElementById('btn-divide');
let btnMod = document.getElementById('btn-mod');

// Tag p for the result
let result = document.querySelector('.p-result');

// call the function
eventListener();

// create the function
function eventListener() {
  btnSum.addEventListener('click',sum);
  btnSubstract.addEventListener('click',substract);
  btnMultiply.addEventListener('click',multiply);
  btnDivide.addEventListener('click',divide);
  btnMod.addEventListener('click',mod);
}

function sum() {
  let num1 = parseFloat(number1.value);
  let num2 = parseFloat(number2.value);
  result.textContent = `Yor result is: ${num1+num2}`;
  result.style.visibility = 'visible';
  result.style.background = 'rgb(233, 182, 106)';
}

function substract() {
  let num1 = parseFloat(number1.value);
  let num2 = parseFloat(number2.value);
  result.textContent = `Yor result is: ${num1-num2}`;
  result.style.visibility = 'visible';
  result.style.background = 'rgb(233, 182, 106)';
}

function multiply() {
  let num1 = parseFloat(number1.value);
  let num2 = parseFloat(number2.value);
  result.textContent = `Yor result is: ${num1*num2}`;
  result.style.visibility = 'visible';
  result.style.background = 'rgb(233, 182, 106)';
}

function divide(e) {
  e.preventDefault();
  let num1 = parseFloat(number1.value);
  let num2 = parseFloat(number2.value);
  if(num2 !== 0) {
    result.textContent = `Yor result is: ${num1/num2}`;
    result.style.background = 'rgb(233, 182, 106)';
  } else {
    result.textContent = `Number 2 can't be 0`;
    result.style.background = '#FFBDEC';    
  }

  
  
}

function mod(e) {
  e.preventDefault();
  let num1 = parseFloat(number1.value);
  let num2 = parseFloat(number2.value);
  if(num2 !== 0) {
    result.textContent = `Yor result is: ${num1%num2}`;
    result.style.background = 'rgb(233, 182, 106)';
  } else {
    result.textContent = `Number 2 can't be 0`;
    result.style.background = '#FFBDEC';    
  }
}